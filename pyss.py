import os
import shutil

from bottle import template
import yaml
import mistune

def clean_build(build_dir):
    try:
        shutil.rmtree(build_dir)
    except FileNotFoundError:
        pass

def file_data(filename):
    data = ""
    with open(filename, 'r') as openFile:
        data = openFile.read()
    data = data.split("---")[0]
    data = yaml.load(data)
    return data

def cp_file(from_name, to_name):
    os.makedirs(os.path.dirname(to_name))
    shutil.copyfile(from_name, to_name)

def template_data(filename):
    td = ""
    with open(filename, 'r') as openFile:
        td = openFile.read()
    td = ''.join(td.split("---")[1:])
    return td

def render_markdown(from_name, to_name):
    os.makedirs(os.path.dirname(to_name))
    text = ""
    with open(from_name, 'r') as openFile:
        text = openFile.read()
    text = ''.join(text.split("---")[1:])
    data = file_data(from_name)
    md = mistune.markdown(text)
    outdata = template(md, **data)
    with open(to_name[:-4].replace(".md",".html"), 'w+') as openFile:
        openFile.write(outdata)

def render_file(from_name, to_name):
    os.makedirs(os.path.dirname(to_name))
    data = file_data(from_name)
    td = template_data(from_name)
    outdata = template(td, **data)
    with open(to_name[:-4], "w+") as openFile:
        openFile.write(outdata)

def render_files(src_dir, build_dir):
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            from_name = root + "/" + file
            to_name = root.replace(src_dir, build_dir) + "/" + file
            if file.endswith((".md.tpl")):
                render_markdown(from_name, to_name)
            elif file.endswith((".tpl")):
                render_file(from_name, to_name)
            else:
                cp_file(from_name, to_name)

if __name__ == "__main__":
    clean_build("build")
    render_files("src", "build")
